package com.a11sight.IIsightsdkdemo.fragments

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.a11sight.IIsightsdkdemo.R
import com.a11sight.IIsightsdkdemo.adapters.CallHistoryadapter
import com.a11sight.IIsightsdkdemo.adapters.UserActivity
import com.a11sight.IIsightsdkdemo.extensions.getDate
import com.a11sight.IIsightsdkdemo.listeners.ListItemClick
import com.a11sight.IIsightsdkdemo.models.Call
import com.a11sight.IIsightsdkdemo.models.CallHistoryResponseModel
import com.a11sight.IIsightsdkdemo.models.User
import com.a11sight.IIsightsdkdemo.models.UserResponseModel
import com.a11sight.IIsightsdkdemo.utils.ExtrasUtil
import com.a11sight.IIsightsdkdemo.utils.UrlUtils
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.elevensight.sdk.models.Session
import kotlinx.android.synthetic.main.call_history_fragment.*
import java.util.*

class CallHistoryFragment : Fragment(), ListItemClick {


    lateinit var adapter: CallHistoryadapter
    lateinit var userList: List<User>
    var userIdInt = -1

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.call_history_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initList()

        val userId = Session.getInstance().userId

        userIdInt = userId.toInt()

        getCalls(userId)

    }

    private fun initList() {
        historyList.layoutManager = LinearLayoutManager(context)

        adapter = CallHistoryadapter(listOf(), this)
        historyList.adapter = adapter
    }

    private fun getCalls(userId: String) {
        AndroidNetworking.get(UrlUtils.BASE_URL + userId + UrlUtils.CALLS)
            .addHeaders("S-Auth-Token", "huj5acult77jpmx3813hr9k2")
            .addHeaders("Content-Type", "application/json")
            .build()
            .getAsObject(
                CallHistoryResponseModel::class.java,
                object : ParsedRequestListener<CallHistoryResponseModel> {
                    override fun onResponse(callResponse: CallHistoryResponseModel) {
                        getUsers(callResponse.calls)
                    }

                    override fun onError(anError: ANError) {
                        // handle error
                    }
                })
    }

    private fun getUsers(calls: List<Call>) {
        AndroidNetworking.get(UrlUtils.USERS)
            .addHeaders("S-Auth-Token", "huj5acult77jpmx3813hr9k2")
            .addHeaders("Content-Type", "application/json")
            .build()
            .getAsObject(UserResponseModel::class.java, object : ParsedRequestListener<UserResponseModel> {
                override fun onResponse(userResponse: UserResponseModel) {

                    userList = userResponse.users
                    addDisplayNames(calls, userResponse.users)

                }

                override fun onError(anError: ANError) {
                    // handle error
                }
            })
    }

    private fun addDisplayNames(calls: List<Call>, users: List<User>) {

        val userIdMap = mutableMapOf<Int, String>()

        repeat(users.size) {
            userIdMap[users[it].id] = users[it].profile.displayName
        }


        repeat(calls.size) {

            //Date Format

            calls[it].createdAt.getDate()?.apply {

                val calendar: Calendar = Calendar.getInstance()
                calendar.time = this
                calls[it].displayDate = calendar.get(Calendar.DAY_OF_MONTH).toString() + calendar.getDisplayName(
                    Calendar.MONTH,
                    Calendar.SHORT,
                    Locale.getDefault()
                )
            }

            when {
                calls[it].callerUserId == userIdInt -> {
                    calls[it].callDisplayName = userIdMap[calls[it].calleeUserId]
                    calls[it].type = getString(R.string.outgoing)
                }
                calls[it].callerUserId != null -> {
                    calls[it].type = getString(R.string.incoming)
                    calls[it].callDisplayName = userIdMap[calls[it].callerUserId]
                }
                else -> {
                    calls[it].callDisplayName = getString(R.string.anonymous)
                    calls[it].type = getString(R.string.incoming)
                }
            }

        }

        adapter.setList(calls)

    }

    override fun onItemClick(position: Int) {

        val intent = Intent(context, UserActivity::class.java)
        val call = adapter.getCall(position)

        when {

            call.callerUserId == userIdInt -> {
                val user = findUser(call.calleeUserId)
                user?.let {
                    addUserData(intent, it)
                    addCallData(intent, call)
                }
            }

            call.callerUserId != null -> {
                val user = findUser(call.callerUserId)
                user?.let {
                    addUserData(intent, it)
                    addCallData(intent, call)
                }
            }

            else -> {
                //Anonymous
                intent.putExtra(ExtrasUtil.NAME, getString(R.string.anonymous))
                addCallData(intent, call)
            }


        }


        startActivity(intent)

    }

    private fun addUserData(intent: Intent, user: User) {
        intent.putExtra(ExtrasUtil.NAME, user.profile.fullName)
        intent.putExtra(ExtrasUtil.EMAIL, user.email)
        intent.putExtra(ExtrasUtil.BUTTON_ID, user.buttons[0].buttonId)
    }

    private fun addCallData(intent: Intent, call: Call) {

        val calendar: Calendar = Calendar.getInstance()

        call.createdAt.getDate()?.let {
            calendar.time = it

            val date = calendar.get(Calendar.DAY_OF_MONTH).toString() + " " + calendar.getDisplayName(
                Calendar.MONTH,
                Calendar.SHORT,
                Locale.getDefault()
            ) + calendar.get(Calendar.YEAR)

            val time = calendar.get(Calendar.HOUR).toString() + ":" + calendar.get(Calendar.MINUTE)

            intent.putExtra(ExtrasUtil.CALL_DATE, date)
            intent.putExtra(ExtrasUtil.CALL_TIME, time)
            intent.putExtra(ExtrasUtil.CALL_TYPE, call.type)

        }


    }

    private fun findUser(id: Int): User? {
        repeat(userList.size) {
            if (userList[it].id == id) {
                return userList[it]
            }
        }

        return null
    }

}
