package com.a11sight.IIsightsdkdemo.fragments

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.a11sight.IIsightsdkdemo.R
import com.a11sight.IIsightsdkdemo.SDKManager
import com.a11sight.IIsightsdkdemo.activities.RegisterActivity

import kotlinx.android.synthetic.main.more_fragment.*

class MoreFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.more_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        logoutButton.setOnClickListener {
            SDKManager.getSDKManager().logoutUser({
                // Success
                val intent = Intent(context, RegisterActivity::class.java)
                startActivity(intent)
                activity?.finish()

            }, {
                //Fail

            })
        }

    }

}