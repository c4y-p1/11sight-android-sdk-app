package com.a11sight.IIsightsdkdemo.fragments

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.a11sight.IIsightsdkdemo.R
import com.a11sight.IIsightsdkdemo.adapters.ContactsAdapter
import com.a11sight.IIsightsdkdemo.adapters.UserActivity
import com.a11sight.IIsightsdkdemo.listeners.ListItemClick
import com.a11sight.IIsightsdkdemo.models.User
import com.a11sight.IIsightsdkdemo.models.UserResponseModel
import com.a11sight.IIsightsdkdemo.utils.ExtrasUtil
import com.a11sight.IIsightsdkdemo.utils.UrlUtils
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.androidnetworking.interfaces.ParsedRequestListener
import kotlinx.android.synthetic.main.contacts_fragment.*
import org.json.JSONObject


class ContactsFragment : Fragment(), ListItemClick {

    lateinit var adapter: ContactsAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.contacts_fragment, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initList()

        AndroidNetworking.get(UrlUtils.USERS)
            .addHeaders("S-Auth-Token", "huj5acult77jpmx3813hr9k2")
            .addHeaders("Content-Type", "application/json")
            .build()
            .getAsObject(UserResponseModel::class.java, object : ParsedRequestListener<UserResponseModel> {
                override fun onResponse(user: UserResponseModel) {
                    // do anything with response
                    adapter.setList(user.users)
                }

                override fun onError(anError: ANError) {
                    // handle error
                }
            })


    }

    private fun initList(){
        contactsList.layoutManager = LinearLayoutManager(context)

        adapter = ContactsAdapter(listOf(), this)
        contactsList.adapter = adapter
    }

    override fun onItemClick(position: Int) {

        val user = adapter.getUser(position)

        val intent = Intent(context, UserActivity::class.java)
        intent.putExtra(ExtrasUtil.NAME, user.profile.fullName)
        intent.putExtra(ExtrasUtil.EMAIL, user.email)
        intent.putExtra(ExtrasUtil.BUTTON_ID, user.buttons[0].buttonId)

        startActivity(intent)


    }

}