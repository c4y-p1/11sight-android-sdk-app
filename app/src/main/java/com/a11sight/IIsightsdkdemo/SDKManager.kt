package com.a11sight.IIsightsdkdemo

import android.app.Activity
import android.app.Application
import android.content.Intent
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.gsonparserfactory.GsonParserFactory
import com.elevensight.sdk.sdk.IISightSDKManager
import com.inrista.loggliest.Loggly

class SDKManager : Application(), IISightSDKManager.ICallback {


    companion object {
        private lateinit var sdkManager: IISightSDKManager
        fun getSDKManager(): IISightSDKManager {
            return sdkManager
        }
    }

    override fun onCreate() {
        super.onCreate()

        // Initialize network library (Android Networking library not include SDK, added from this demo app)
        AndroidNetworking.initialize(applicationContext);
        AndroidNetworking.setParserFactory(GsonParserFactory())

        //Initialize Loggly (Must add this initialization)
        Loggly.with(this, "")
            .appendDefaultInfo(true)
            .uploadIntervalLogCount(5)
            .uploadIntervalSecs(5)
            .maxSizeOnDisk(500000)
            .init()

        //Initialize SDK Manager (Must)
        try {
            IISightSDKManager.build(this)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        sdkManager = IISightSDKManager.getInstance(this)

    }

    //SDKManager Callback Implementation
    override fun process(p0: Any?) {
        val i = Intent(applicationContext, Activity::class.java)
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        applicationContext.startActivity(i)
    }


}