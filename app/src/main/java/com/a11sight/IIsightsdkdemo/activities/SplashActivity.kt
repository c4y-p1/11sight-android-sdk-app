package com.a11sight.IIsightsdkdemo.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.a11sight.IIsightsdkdemo.R
import com.a11sight.IIsightsdkdemo.SDKManager

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_activity)

        if (SDKManager.getSDKManager().isLoggedIn(this)) {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        } else {
            startActivity(Intent(this, RegisterActivity::class.java))
            finish()
        }

    }

}