package com.a11sight.IIsightsdkdemo.activities

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import com.a11sight.IIsightsdkdemo.R
import com.a11sight.IIsightsdkdemo.adapters.ViewPagerAdapter
import com.a11sight.IIsightsdkdemo.fragments.CallHistoryFragment
import com.a11sight.IIsightsdkdemo.fragments.ContactsFragment
import com.a11sight.IIsightsdkdemo.fragments.MoreFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        val viewPager = findViewById<ViewPager>(R.id.viewpager)
        setupViewPager(viewPager)

        val tabLayout = findViewById<TabLayout>(R.id.tabs)
        tabLayout.setupWithViewPager(viewPager)

        tabLayout.getTabAt(0)?.text = "Call History"
        tabLayout.getTabAt(1)?.text = "Contacts"
        tabLayout.getTabAt(2)?.text = "More"

    }

    private fun setupViewPager(viewPager: ViewPager) {

        val adapter = ViewPagerAdapter(supportFragmentManager)
        adapter.addFragment(CallHistoryFragment(), "Call History")
        adapter.addFragment(ContactsFragment(), "Contacts")
        adapter.addFragment(MoreFragment(), "More")
        viewPager.adapter = adapter
    }
}
