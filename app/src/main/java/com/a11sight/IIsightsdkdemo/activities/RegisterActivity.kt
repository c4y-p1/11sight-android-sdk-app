package com.a11sight.IIsightsdkdemo.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import com.a11sight.IIsightsdkdemo.R
import com.a11sight.IIsightsdkdemo.SDKManager
import com.elevensight.sdk.sdk.IISightSDKManager
import kotlinx.android.synthetic.main.register_activity.*

class RegisterActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.register_activity)



        loginButton.setOnClickListener {
            attemptLogin()
        }
    }

    private fun attemptLogin() {

        // Reset errors.
        emailTextView.error = null
        passwordEditText.error = null

        // Store values at the time of the login attempt.
        val email = emailTextView.text.toString()
        val password = passwordEditText.text.toString()

        var cancel = false
        var focusView: View? = null

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            passwordEditText.error = getString(R.string.error_invalid_password)
            focusView = passwordEditText
            cancel = true
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            emailTextView.error = getString(R.string.error_field_required)
            focusView = emailTextView
            cancel = true
        } else if (!isEmailValid(email)) {
            emailTextView.error = getString(R.string.error_invalid_email)
            focusView = emailTextView
            cancel = true
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView?.requestFocus()
        } else {

            // Login
            SDKManager.getSDKManager()
                .loginUser(email, password, applicationContext, {

                    //Success Login
                    startActivity(Intent(this, MainActivity::class.java))
                    finish()

                }, {
                    //Fail Login
                })
        }
    }


    private fun isEmailValid(email: String): Boolean {
        //TODO: Replace this with your own logic
        return email.contains("@")
    }

    private fun isPasswordValid(password: String): Boolean {
        //TODO: Replace this with your own logic
        return password.length > 4
    }


}
