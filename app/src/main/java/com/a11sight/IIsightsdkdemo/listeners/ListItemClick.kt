package com.a11sight.IIsightsdkdemo.listeners

interface ListItemClick {
    fun onItemClick(position: Int)
}