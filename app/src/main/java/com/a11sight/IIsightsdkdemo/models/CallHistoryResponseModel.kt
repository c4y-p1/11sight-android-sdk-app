package com.a11sight.IIsightsdkdemo.models

import com.google.gson.annotations.SerializedName

data class CallHistoryResponseModel(
    val count: Int,
    @SerializedName("total_count")
    val totalCount: Int,
    val calls: List<Call>
)

data class Call(
    val id: String,
    @SerializedName("created_at")
    val createdAt: String,
    val state: String,
    val duration: Int,
    @SerializedName("ending_time")
    val endingTime: String,
    @SerializedName("estimated_start_time")
    val estimatedStartTime: String,
    @SerializedName("callee_user_id")
    val calleeUserId: Int,
    @SerializedName("caller_user_id")
    val callerUserId: Int?,
    var callDisplayName: String?,
    var type: String?,
    var displayDate: String?
)
