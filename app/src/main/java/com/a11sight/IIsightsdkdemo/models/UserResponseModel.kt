package com.a11sight.IIsightsdkdemo.models

import com.google.gson.annotations.SerializedName

data class UserResponseModel(
    val count: Int,
    @SerializedName("total_count")
    val totalCount: Int,
    val users: List<User>
)

data class User(
    val id: Int,
    val email: String,
    @SerializedName("time_zone")
    val timeZone: String,
    val profile: Profile,
    val buttons: List<Button>
)

data class Profile(
    @SerializedName("fullname")
    val fullName: String,
    @SerializedName("display_name")
    val displayName: String
)

data class Button(
    @SerializedName("button_id")
    val buttonId: String,
    val name: String
)