package com.a11sight.IIsightsdkdemo.utils

class ExtrasUtil {
    companion object {
        const val BUTTON_ID = "button_id"
        const val NAME = "name"
        const val EMAIL = "email"
        const val CALL_DATE = "call_date"
        const val CALL_TIME = "call_time"
        const val CALL_TYPE = "call_type"
    }
}