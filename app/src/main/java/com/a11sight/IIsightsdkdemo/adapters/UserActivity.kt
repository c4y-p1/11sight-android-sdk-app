package com.a11sight.IIsightsdkdemo.adapters

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.a11sight.IIsightsdkdemo.R
import com.a11sight.IIsightsdkdemo.SDKManager
import com.a11sight.IIsightsdkdemo.utils.ExtrasUtil
import com.elevensight.sdk.models.OutgoingCallType
import kotlinx.android.synthetic.main.user_activity.*

class UserActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.user_activity)

        val name = intent.extras.getString(ExtrasUtil.NAME)
        val email = intent.extras.getString(ExtrasUtil.EMAIL)
        val buttonId = intent.extras.getString(ExtrasUtil.BUTTON_ID)

        val callDate = intent.extras.getString(ExtrasUtil.CALL_DATE)
        val callTime = intent.extras.getString(ExtrasUtil.CALL_TIME)
        val callType = intent.extras.getString(ExtrasUtil.CALL_TYPE)


        if (email == null || buttonId == null) {
            userEmailText.visibility = View.GONE
            phoneButton.visibility = View.GONE
            chatButton.visibility = View.GONE
            videoButton.visibility = View.GONE
        } else {
            userEmailText.text = email
        }

        if (callDate != null && callTime != null && callType != null) {
            userCallDate.text = callDate
            userCallTime.text = callTime
            userCallType.text = callType
        } else {
            callLayout.visibility = View.GONE
        }

        userBadgeText.text = name[0].toString()
        userNameText.text = name

        phoneButton.setOnClickListener {
            SDKManager.getSDKManager().makeCall(this, OutgoingCallType.AUDIO, buttonId)
        }

        chatButton.setOnClickListener {
            SDKManager.getSDKManager().makeCall(this, OutgoingCallType.CHAT, buttonId)
        }

        videoButton.setOnClickListener {
            SDKManager.getSDKManager().makeCall(this, OutgoingCallType.VIDEO, buttonId)
        }

    }

}