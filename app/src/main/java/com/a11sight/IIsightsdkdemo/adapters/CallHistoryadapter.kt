package com.a11sight.IIsightsdkdemo.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.a11sight.IIsightsdkdemo.R
import com.a11sight.IIsightsdkdemo.listeners.ListItemClick
import com.a11sight.IIsightsdkdemo.models.Call


class CallHistoryadapter(var callList: List<Call>, var clickListener: ListItemClick) :
    RecyclerView.Adapter<CallHistoryadapter.HistoryViewHolder>() {

    fun setList(callList: List<Call>) {
        this.callList = callList
        notifyDataSetChanged()
    }

    fun getCall(position: Int): Call = callList[position]

    override fun getItemCount(): Int = callList.size

    override fun onBindViewHolder(holder: HistoryViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.call_history_item, parent, false)
        return HistoryViewHolder(view)
    }

    inner class HistoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        private var callerTextView: TextView = itemView.findViewById(R.id.callerText) as TextView
        private var callTypeTextView: TextView = itemView.findViewById(R.id.typeText) as TextView
        private var dateTextView: TextView = itemView.findViewById(R.id.callDate) as TextView

        fun bind(position: Int) {
            val call = callList[position]

            callerTextView.text = call.callDisplayName
            callTypeTextView.text = call.type
            dateTextView.text = call.displayDate

            itemView.setOnClickListener {
                clickListener.onItemClick(position)
            }

        }


    }
}