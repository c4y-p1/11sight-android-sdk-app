package com.a11sight.IIsightsdkdemo.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.a11sight.IIsightsdkdemo.R
import com.a11sight.IIsightsdkdemo.listeners.ListItemClick
import com.a11sight.IIsightsdkdemo.models.User


class ContactsAdapter(var contacts: List<User>, var clickListener: ListItemClick) :
    RecyclerView.Adapter<ContactsAdapter.ContactViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.contact_item, parent, false)
        return ContactViewHolder(view)
    }

    fun setList(contacts: List<User>) {
        this.contacts = contacts
        notifyDataSetChanged()
    }

    fun getUser(position: Int): User = contacts[position]

    override fun getItemCount(): Int = contacts.size

    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        holder.bind(position)
    }


    inner class ContactViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private var mailTextView: TextView = itemView.findViewById(R.id.contactMail) as TextView
        private var nameTextView: TextView = itemView.findViewById(R.id.contactName) as TextView


        fun bind(position: Int) {

            val user = contacts[position]

            nameTextView.text = user.profile.fullName
            mailTextView.text = user.email

            itemView.setOnClickListener {
                clickListener.onItemClick(position)
            }


        }

    }

}