package com.a11sight.IIsightsdkdemo.services

import android.util.Log
import com.elevensight.sdk.sdk.IISightSDKManager
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService

class IISightFirebaseInstanceIdService : FirebaseInstanceIdService() {

    override fun onTokenRefresh() {

        val refreshedToken = FirebaseInstanceId.getInstance().token
        IISightSDKManager.getInstance(this).sendRegistrationToServer(refreshedToken)
        Log.d(TAG, "Refreshed token: " + refreshedToken!!)
    }

    companion object {

        private val TAG = "11_FCM_PUSH"
    }

}