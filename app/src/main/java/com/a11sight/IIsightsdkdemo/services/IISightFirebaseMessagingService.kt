package com.a11sight.IIsightsdkdemo.services

import android.util.Log
import com.elevensight.sdk.sdk.IISightSDKManager
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class IISightFirebaseMessagingService : FirebaseMessagingService() {


    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        val check = IISightSDKManager.getInstance(this).isIISightNotification(remoteMessage)
        if (check) {
            IISightSDKManager.getInstance(this).handleIISightNotification(remoteMessage)
        }
    }

    override fun onDeletedMessages() {
        Log.e(TAG, "Message Deleted: ")
    }

    companion object {
        private val TAG = "11_FCM_PUSH"
    }
}