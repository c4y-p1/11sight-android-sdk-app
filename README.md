﻿﻿# Read Me
### 11Sight SDK Android Demo

This codebase is a simple example of  **11Sight SDK**  usage ***not***  SDK. If you need SDK documentation, here is the Android SDK [docs](https://11sight.com/sdk/docs/#how-to-start-with-app-sdk-android). 
Our demo uses 11Sight SDK's fundamental features.
- Login
- Make outgoing calls (Video/Audio/Text)
- Receive calls (Video/Audio/Text)
- Display Call History
- Display Call Detail

### Initialize SDK Manager

***SDKManager.kt***
```
override fun onCreate() {  
    super.onCreate()  
  
    // Just added for this demo
    AndroidNetworking.initialize(applicationContext);  
    AndroidNetworking.setParserFactory(GsonParserFactory())  
  
    //Initialize Loggly (Must add this initialization)  
    Loggly.with(this, "")  
        .appendDefaultInfo(true)  
        .uploadIntervalLogCount(5)  
        .uploadIntervalSecs(5)  
        .maxSizeOnDisk(500000)  
        .init()  
  
    //Initialize SDK Manager (Must)  
	try {  
        IISightSDKManager.build(this)  
    } catch (e: Exception) {  
        e.printStackTrace()  
    }  
  
    sdkManager = IISightSDKManager.getInstance(this)  
  
}
```

### Services
SDK uses firebase services, you need to add these services between **application** tags in

***AndroidManifest.xml***
```
<service  
  android:exported="true"  
  android:enabled="true"  
  android:name=".services.IISightFirebaseMessagingService">
	<intent-filter> 
		<action android:name="com.google.firebase.MESSAGING_EVENT"/>  
	</intent-filter>
</service>
  
<service  
  android:name=".services.IISightFirebaseInstanceIdService"  
  android:enabled="true"  
  android:exported="true">  
	 <intent-filter> 
		 <action android:name="com.google.firebase.INSTANCE_ID_EVENT" />  
	 </intent-filter>
</service>
```

Also you need to create a service class, which is look like

***IISightFirebaseMessagingService.kt***

```
class IISightFirebaseMessagingService : FirebaseMessagingService() {  
  
  
    override fun onMessageReceived(remoteMessage: RemoteMessage?) {  
        val check = IISightSDKManager.getInstance(this).isIISightNotification(remoteMessage)  
        if (check) {  
            IISightSDKManager.getInstance(this).handleIISightNotification(remoteMessage)  
        }  
    }  
  
    override fun onDeletedMessages() {  
        Log.e(TAG, "Message Deleted: ")  
    }  
  
    companion object {  
        private val TAG = "11_FCM_PUSH"  
  }  
}
```

***IISightFirebaseInstanceIdService***

```
class IISightFirebaseInstanceIdService : FirebaseInstanceIdService() {  
  
    override fun onTokenRefresh() {  
  
        val refreshedToken = FirebaseInstanceId.getInstance().token  
	    IISightSDKManager.getInstance(this).sendRegistrationToServer(refreshedToken)  
        Log.d(TAG, "Refreshed token: " + refreshedToken!!)  
    }  
  
    companion object {  
  
        private val TAG = "11_FCM_PUSH"  
  }  
  
}
```


